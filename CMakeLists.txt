cmake_minimum_required (VERSION 3.15)
project (PhoneMinutesTest)

# targets
add_executable (PhoneMinutesTest PhoneMinutesTest.cpp PhoneMinutes.cpp)

# compilation settings
set (CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -std=c++17 -Wall -Wextra -fPIC -pedantic")
set (CMAKE_CXX_FLAGS_DEBUG   "-O0 -ggdb")
set (CMAKE_CXX_FLAGS_RELEASE "-O3") # -0g or -03
set (CMAKE_CXX_STANDARD 17)
include_directories           (.)
