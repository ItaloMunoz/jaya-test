/**
 * @file main.cpp
 *
 * @brief Implementation of program entry point
 *
 * @author   Ítalo Muñoz
 * @date     May, 2020
 * @revision $Revision: 1$
 *
 * @copyright 2020 imunoz. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of imunoz. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to imunoz.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

/* INCLUDES ******************************************************************/

#include <cassert> // assert
#include "PhoneMinutes.h"

/* IMPLEMENTATION ************************************************************/

int main()
{
// Define a C++ header file with variable(s) to store information related to phone numbers and their aggregate minutes.
// write 2 funcitons:
// add() consume duration
// printTop() print top 'n' numbers sorted based on phoneNumber of aggregate minutes
// Example calls:
// add("5550001", 4);
// add("5550005", 7);
// add("5550001", 5);
// add("5550001", 2);
// add("5550003", 1);
// add("5550004", 2);
// add("5550007", 2);

// printTop(2);
// prints only top 2 numbers with their minutes
// >55550001 11
// >55550005 7
  
  PhoneMinutes phoneMinutes;
  
  phoneMinutes.add("5550001", 4);
  phoneMinutes.add("5550005", 7);
  phoneMinutes.add("5550001", 5);
  phoneMinutes.add("5550001", 2);
  phoneMinutes.add("5550003", 1);
  phoneMinutes.add("5550004", 2);
  phoneMinutes.add("5550007", 2);

  PhoneMinutes::TopElement_t expectedResult =
  {
    {11, "5550001"},
    {7, "5550005"}
  };
  assert(expectedResult == phoneMinutes.getTopElements(2));
  phoneMinutes.printTop(2);

  phoneMinutes.add("5550003", 19);
  expectedResult[20] = "5550003";
  assert(expectedResult == phoneMinutes.getTopElements(3));
  phoneMinutes.printTop(3);

  return 0;
}