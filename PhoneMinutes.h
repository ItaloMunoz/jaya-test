/**
 * @file PhoneMinutes.h
 *
 * @brief Declaration of PhoneMinutes class
 *
 * @author   Ítalo Muñoz
 * @date     May, 2020
 * @revision $Revision: 1$
 *
 * @copyright 2020 imunoz. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of imunoz. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to imunoz.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

#pragma once

/* INCLUDES ******************************************************************/

#include <functional> // greater
#include <map>
#include <set>
#include <string>

/* CLASS DECLARATION *********************************************************/

/**
 * @brief PhoneMinutes ...
 *
 */
class PhoneMinutes
{
public:
  using TopElement_t = std::map<int, std::string, std::greater<int>>;

	/**
	 * @brief
	 *
	 */
  void add(const std::string& phoneNumber, int minutes);

	/**
	 * @brief
	 *
	 */
  void printTop(size_t numberOfTopElemens = 10);

	/**
	 * @brief
	 *
	 */
  TopElement_t getTopElements(size_t numberOfTopElemens = 10);

	/**
	 * @brief
	 *
	 */
  size_t size() const;

private:
  std::map<std::string, int>           m_minutes;
  std::map<int, std::set<std::string>> m_sorted_minutes;
};
