#!/bin/bash

IN=${1:-test.cpp PhoneMinutes.cpp}

echo g++ -g -O0 -std=c++11 -Wall -Wextra -fPIC -pedantic $IN
     g++ -g -O0 -std=c++11 -Wall -Wextra -fPIC -pedantic $IN
