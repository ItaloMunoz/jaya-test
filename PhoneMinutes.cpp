/**
 * @file PhoneMinutes.cpp
 *
 * @brief Implementation of PhoneMinutes class
 *
 * @author   Ítalo Muñoz
 * @date     May, 2020
 * @revision $Revision: 1$
 *
 * @copyright 2020 imunoz. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of imunoz. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to imunoz.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

/* INCLUDES ******************************************************************/

#include <cassert> // assert
#include <iostream> // cout
#include "PhoneMinutes.h"

/* CLASS IMPLEMENTATION ******************************************************/

void PhoneMinutes::add(const std::string& phoneNumber, int minutes)
{
  auto phoneNumberPos = m_minutes.find(phoneNumber);
  if (phoneNumberPos == m_minutes.end()) // not found
  {
    m_minutes[phoneNumber] += minutes;
    m_sorted_minutes[minutes].insert(phoneNumber);
    return;
  }

  auto previousMinutes = phoneNumberPos->second;
  m_minutes[phoneNumber] += minutes;
  
  auto minutesPos = m_sorted_minutes.find(previousMinutes);
  if (minutesPos != m_sorted_minutes.end())
  {
    m_sorted_minutes.erase(minutesPos);
    m_sorted_minutes[minutes + minutesPos->first].insert(phoneNumber);
  }
  else
  {
    assert(false);
  }
}

void PhoneMinutes::printTop(size_t numberOfTopElemens)
{
  auto topElements = getTopElements(numberOfTopElemens);
  for (auto& elem : topElements)
  {
    std::cout << elem.second << " " << elem.first << std::endl;
  }
}

PhoneMinutes::TopElement_t PhoneMinutes::getTopElements(size_t numberOfTopElemens)
{
  TopElement_t topElements;
  size_t addedNumbers = 0;
  for
  (
    auto sorted_minute  = m_sorted_minutes.rbegin() ;
    sorted_minute != m_sorted_minutes.rend() && addedNumbers < numberOfTopElemens;
    ++sorted_minute
  )
  {
    for (auto& phoneNumber : sorted_minute->second)
    {
      topElements[sorted_minute->first] = phoneNumber;
      addedNumbers++;
      if (addedNumbers >= numberOfTopElemens)
        break;
    }
  }

  return topElements;
}

size_t PhoneMinutes::size() const
{
  // assert(m_minutes.size() == m_sorted_minutes.size());
  return m_minutes.size();
}
